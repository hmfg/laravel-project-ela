<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;

class OrderController extends Controller
{
    public function order()

    {
        $orders=Order::all();
        return view('backend/orders/order-list',compact('orders'));
    }

    public function edit($id)
    {
        $orders= Order::findOrFail($id);
        return view('backend/orders/order-edit' ,compact('orders'));
    }

    
    public function pdf()
        {
            $pdf = PDF ::loadView('backend.orders.pdf');
            return $pdf->download('Order-List.pdf');
        }


        
}
