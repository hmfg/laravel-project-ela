<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        echo 'Running database seeder';

        // DB::table('categories')->insert([
        //     'title' => 'Fashion',
        //     'description' => 'This is test description for fashion'
        // ]);

        // Category::create(
        //     [
        //         'title' => 'Footwear',
        //         'description' => 'This is test description'
        //     ]
        // );

        // User::factory(10)->create();

        $this->call([
            RolesTableSeeder::class,
            CategoriesTableSeeder::class,
            ProductsTableSeeder::class
        ]);
    }
}
