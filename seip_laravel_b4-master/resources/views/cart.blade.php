<x-frontend.layouts.master>


    <div class="bg-light p-5 rounded">
        <div class="card">
            <div class="card-header"></div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#SL</th>
                            <th>Product</th>
                            <th>Qty</th>
                            <th>Unit Price</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $ItemTotalPrice = 0;
                        $totalPrice = 0;
                        @endphp
                        @foreach ($cartItems as $item)

                        @php
                        $ItemTotalPrice = $item->qty * $item->unit_price;
                        $totalPrice += $ItemTotalPrice;
                        @endphp

                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->product->title }}</td>
                            <td>{{ $item->qty }}</td>
                            <td>{{ $item->unit_price }}</td>
                            <td>{{ $ItemTotalPrice }}</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td align="right" colspan="4">Total Price</td>
                            <td>{{ $totalPrice }}</td>

                        </tr>
                    </tbody>
                </table>

                <form>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1">
                    </div>
                    <div class="mb-3">
                        <label for="phone" class="form-label">Phone</label>
                        <input type="text" name="phone" class="form-control" id="phone" >
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" name="address" class="form-label">Address</label>
                        <textarea class="form-control">

                        </textarea>
                    </div>
                    <button class="btn btn-primary" type="submit">Place Order</button>
                </form>
            </div>
        </div>
    </div>
</x-frontend.layouts.master>