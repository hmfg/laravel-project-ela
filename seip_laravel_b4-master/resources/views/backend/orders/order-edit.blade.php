<x-backend.layouts.master>
<form method="post"  action="{{route('order.edit',['id'=>$orders->id])}}">
@csrf
@method('patch')
 <div class="mb-3">
 <label for="order_status" class="form-label"> Order Status</label>

<select class="form-select" id="order_status" name="order_status">
<option selected>Pending</option>
<option>Delivered</option>
</select>
 </div>

<div class=" d-grid col-3 mx-auto mt-2">
<button type="sumbit" class="btn btn-outline-success">Update Order</button>
</div>



</form>
</x-backend.layouts.master>
