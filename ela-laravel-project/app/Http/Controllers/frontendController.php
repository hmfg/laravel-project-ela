<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class frontendController extends Controller
{
    public function welcome()
    {
        $products = Product::paginate(10);
        return view('frontend.welcome',compact('products'));
    }
}
