<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Barryvdh\DomPDF\Facade\Pdf;

class ProductController extends Controller
{
    public function admin()
    {

        return view('backend.dashboard');
    }
    public function index()
    {

        $products = Product::orderBy('id','desc')->get();
     
        return view('backend.index', compact('products'));
    }
    function show($id)
    {
        $products = Product::findOrFail($id);

        return view('backend.show', compact('products'));
    }
    public function create()
    {
        return view('backend.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|min:5',
            'description' => 'required',
            'price' => 'required|numeric',
            'image' => 'required|mimes:jpg|min:5|max:2048'
        ]);
        // dd('valided');'

        $data = $request->all();
        if($request->hasFile('image')){
        $image_name=time() . '.' .$request->file('image')->getClientOriginalExtension();
        $image = Image::make($request->file('image'))
        ->resize(300, 200)
        ->save(storage_path().'/app/public/products/'.$image_name);
        $data['image'] = $image_name;
        }
        // dd($image_name); 
        
        Product::create(

            [
                'title' => $data['title'],
                'description' => $data['description'],
                'price' => $data['price'],
                'image'=> $data['image']
            ]
        );
        return redirect()->route('product.list');
    }
    public function edit($id)
    {

        $products = Product::findOrFail($id);
        return view('backend.edit', compact('products'));
    }
    public function update(Request $request, $id)
    {

        $request->validate([
            'title' => 'required|min:5',
            'description' => 'required',
            'price' => 'required|numeric',
            'image' => 'required|min:5|max:2048'
        ]);
        $products = Product::findOrFail($id);

        if($request->hasFile('image')){
            $image_name=time() . '.' .$request->file('image')->getClientOriginalExtension();
            $image = Image::make($request->file('image'))
            ->resize(300, 200)
            ->save(storage_path().'/app/public/products/'.$image_name);
            

        $products->update([
            'title' => $request->title,
            'description' => $request->description,
            'price' => $request->price,
            'image' => $image_name,
        ]);
    }

        return redirect()->route('product.list')->withMessage('Successfully Updated');
    }

    public function destroy($id)
    {
        $products = Product::findOrFail($id);
        $products->delete();

        return redirect()->route('product.list')->withMessage('Successfully Deleted');
    }

    public function trash()
    {
        $products= Product::onlyTrashed()->get();
        return view('backend.trash', compact('products'));
    }
    public function restore($id)
    {
        Product::withTrashed()
               ->where('id', $id )
               ->restore();
        // dd($id);
        return redirect()->route('product.trash')->withMessage('Successfully restore !');
    }

    public function delete($id)
    {
        Product::withTrashed()
              ->where('id', $id)
              ->forceDelete();

              return redirect()->route('product.trash')->withMessage('Successfully Deleted !');
    }

    public function pdf()
        {
            $pdf = PDF ::loadView('backend.pdf');
            return $pdf->download('Product-List.pdf');
        }

}
