<?php

use App\Http\Controllers\frontendController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');


require __DIR__.'/auth.php';

Route::prefix('/admin')->middleware(['auth'])->group(function(){

    Route::get('/deleted-products',[ProductController::class,'trash'])->name('product.trash');

    Route::get('/deleted-products/{id}/restore',[ProductController::class,'restore'])->name('product.restore');

    Route::delete('/deleted-products/{id}',[ProductController::class,'delete'])->name('product.delete');

    Route::get('/product/pdf',[ProductController::class,'pdf'])->name('product.pdf');




    Route::get('/',[ProductController::class,'admin']);
    Route::get('/product/list',[ProductController::class,'index'])->name('product.list');
    Route::get('/product/create',[ProductController::class,'create'])->name('product.create');
    Route::post('/product/store',[ProductController::class,'store'])->name('product.store');
    Route::get('/product/{id}',[ProductController::class,'show'])->name('product.show');
    Route::get('/prdouct/{id}/edit',[ProductController::class,'edit'])->name('product.edit');
    Route::patch('/prdouct/{id}',[ProductController::class,'update'])->name('product.update');
    Route::delete('/product/{id}',[ProductController::class,'destroy'])->name('product.destroy');
    
    
}
);

Route::get('/',[frontendController::class,'welcome'])->name('welcome');


