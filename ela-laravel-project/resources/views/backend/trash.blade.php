<x-master>
   <a href="{{route('product.list')}}" class="btn btn-info mx-3 mb-3">Add Product </a>
   @if(Session::has('message'))
   <p class="text-danger">{{session::get('message')}}</p>
   @endif
   <table class="table table-info table-striped table-hover ">
  <thead>
    <tr>
      <th scope="col">SL</th>
      <th scope="col">Title</th>
      <th scope="col">Description</th>
      <th scope="col">Price</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
     @foreach($products as $product)
    <tr>
      <th scope="row">{{$loop->iteration}}</th>
      <td>{{$product->title}}</td>
      <td>{{Str::limit($product->description,50)}}</td>
      <td>{{$product->price}}</td>
      <td>
          <!-- <a  href="{{route('product.show',['id'=>$product->id])}}"class="btn btn-danger">Show</a> -->

      <a href="{{route('product.restore',['id'=>$product->id])}}"class="btn btn-warning">Restore</a>

      <form action="{{route('product.delete',['id'=>$product->id])}}" method="post" style="display:inline">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-success" onclick="return confirm('Are you sure want to delete?')">Delete</button>
      </form>
      
      
    </tr>
    @endforeach
  </tbody>
</table>
   
</x-master>