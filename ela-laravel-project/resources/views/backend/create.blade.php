<x-master>

<form action="{{route('product.store')}}" method="Post" enctype="multipart/form-data">
    @csrf

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="mb-3">
  <label for="exampleFormControlInput1" class="form-label">Title</label>
  <input type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="Enter Product title">
</div>

<!-- <div class="mb-3">
  <label for="exampleFormControlInput1" class="form-label">Product Category</label>
  <input type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="Enter Product title">
</div> -->
<div class="mb-3">
  <label for="exampleFormControlTextarea1" class="form-label">Description</label>
  <textarea  name="description"class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
</div> 
<div class="mb-3">
  <label for="exampleFormControlInput1" class="form-label">Price</label>
  <input type="mumber" name="price" class="form-control" id="exampleFormControlInput1">
</div>

<div class="mb-3">
  <label for="image" class="form-label">image</label>
  <input type="file" name="image" class="form-control" id="image">
</div> 
<div class="mb-3">
    <button type="submit" class="btn btn-warning">Save</button>
</div>

</form>

      
</x-master>