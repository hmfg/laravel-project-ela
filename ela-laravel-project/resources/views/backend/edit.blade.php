<x-master>


<form action="{{route('product.update',['id'=>$products->id])}}" method="Post">

    @csrf
    @method('patch')

    
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="mb-3">
  <label for="exampleFormControlInput1" class="form-label">Tittle</label>
  <input type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="Enter title" value="{{$products->title}}">
</div>
<div class="mb-3">
  <label for="exampleFormControlTextarea1" class="form-label">Description</label>
  <textarea  name="description"class="form-control" id="exampleFormControlTextarea1"  rows="3" >{{$products->description}}</textarea>
</div> 
<div class="mb-3">
  <label for="exampleFormControlInput1" class="form-label">Price</label>
  <input type="number" name="price" class="form-control" id="exampleFormControlInput1" value="{{$products->price}}">
</div> 
<div class="mb-3">
  <label for="image" class="form-label">image</label>
  <input type="file" name="image" class="form-control" id="image">
</div> 
<div class="mb-3">
    <button type="submit" class="btn btn-warning">Update</button>
</div>

</form>

      

</x-master>