<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ordered_by');
            $table->bigInteger('order_no')->unique();
            $table->string('status', 50)->default('Pending');
            $table->string('phone_no');
            $table->string('email');
            $table->text('shipping_address');
            $table->string('payment_method');
            $table->timestamps();
            $table->foreign('ordered_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
