<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Order;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::all();
        return view('backend.brands.index', compact('brands'));
    
    }   

    public function create()
    {
        return view('backend.brands.create');
    }

    public function store(Request $request)
    {   
        $data=$request->all();
        Brand::create(
            
            [
                'BrandName'=>$data['Brand'],
                'Country'=>$data['country']

            ]
            );
            return redirect()->route('brands.index');
        
    }
    public  function edit($id)
    {
        $brands =Brand::findOrfail($id);
        return view('backend.brands.edit',compact('brands'));
    }

    public function update(Request $request,$id )

    {
       // dd($request['country']);
        $brands =Brand::findOrfail($id);
        $brands ->update([
        
                'BrandName'=>$request['Brand'],
                'Country'=>$request['country']

        ]);
         return redirect()->route('brands.index')->withMessage('Successfully Updated !');
    }

  
    public function destroy($id)
    {
        $brands = Brand::findOrFail($id);
        $brands->delete();

        return redirect()->route('brands.index')->withMessage('Successfully Deleted');
    }
 
}
