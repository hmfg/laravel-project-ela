<x-backend.layouts.master>
    <h1 class="mt-4">Brands</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Brands</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>

            Brands List
            <a class="btn btn-sm btn-primary" href="{{ route('brands.create') }}">Add New</a>
        </div>
        <div class="card-body">

            @if(Session::has('message'))
            <p class="text-danger">{{session::get('message')}}</p>
            @endif


            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>BrandsName</th>
                        <th>Country</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($brands as $brand)
                    <tr>

                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $brand->BrandName }}</td>
                        <td>{{ $brand->Country}} </td>
                        <td>


                            <a href="{{ route('brands.edit', ['id' => $brand->id])}}" class="btn btn-warning btn-success">Edit</a>
                            <form action="{{route('brands.delete',['id =>$brand->id'])}}"method="post"styte="display:inline" >
                            @csrf
                         @method('DELETE')
                            <button type="submit" class="btn btn-success"onclick="return confirm('Are you sure to delete?')">Delete</button>
                            </form>
                        </td>
                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>