<x-backend.layouts.master>
<form action="{{route('brands.update',['id'=>$brands->id])}}" method="post">
    @csrf
    @method('patch')
    
  <div class="mb-3">
    <label for="Brand" class="form-label">Brand Name</label>
    <input type="text"  name="Brand" class="form-control" id="Brand" aria-describedby="emailHelp" value="{{$brands->Brand}}">
    
  </div>
  <div class="mb-3">
    <label for="country" class="form-label">Country</label>
    <input type="text" name="country" class="form-control" id="country" value="{{$brands->country}}">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

      
</x-backend.layouts.master>