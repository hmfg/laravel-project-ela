<x-backend.layouts.master>
<form action="{{route('brands.store')}}" method="post">
    @csrf
  <div class="mb-3">
    <label for="Brand" class="form-label">Brand Name</label>
    <input type="text"  name="Brand" class="form-control" id="Brand" aria-describedby="emailHelp">
    
  </div>
  <div class="mb-3">
    <label for="country" class="form-label">Country</label>
    <input type="text" name="country" class="form-control" id="country">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

      
</x-backend.layouts.master>